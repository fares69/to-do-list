<?php
class TodoL {
    private $item;
    private $done;
    private $id;
    static $count = 0;
//getters
    public function getItem(){
        return $this->item;
    } 
    public function getDone(){
        return $this->done;
    } 
    public function getId(){
        return $this->id;
    }

//setters
    public function setItem($value){
        $this->item =$value;
    }
    public function setDone($value){
        $this->done =$value;
    }

    //constructor
    public function __construct($item, $done = false){
        $this->setItem($item);
        $this->setDone($done);
        $this->id= uniqid();
        self::$count++ ; 
    }
}





?>