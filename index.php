<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="form.css">
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">
    <title>Document</title>
</head>

<body>
    <!-- <div id='initTodolist'>
        <form action='forms/addTodoList.php' method='POST'>
            <input name='initTodoList' type='text' method='forms/addTodoList.php'>
        </form>
    </div> -->
    <div id='forms'>
    <form id='inputform' action='forms/addTodo.php' method='POST'>
                <textarea name="insertToDo" type='text' placeholder='Add your task here :)'></textarea>
                <input type='submit'>
            </form>
        <?php
            // include ('/home/aynn/Simplon/to-do-list/class/TodoList.php');
            include (__DIR__."/classe/todo.php");
            include (__DIR__.'/classe/helpers.php');
            include (__DIR__.'/classe/db.php');

            $bdd = get_co();
            $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $todos = $bdd->query('SELECT * FROM Todos');
            $todos = $todos->fetchAll();


            if (!empty($todos)) {
                foreach ($todos as $data) { ?>
            <div class='retrivedTodo'>
                <form class = 'addDelUpdt' action='forms/updateTodo.php' method='POST'>
                    <input class='checkbox' type='checkbox'>
                    <input name='retrivedTodo' type='text' method='POST' action='forms/updateTodo.php' value='<?php echo $data['Task'];?>'>
                    <input name='retrivedId' type='hidden' method='POST' value='<?php echo $data['ID'];?>'>
                    <input class='floppy' type='image' src='assets/floppy.png' action='forms/updateTodo.php' value='submit' name='save' />
                </form>
                <form action='forms/deleteTodo.php' method='POST'>
                    <input name='retrivedId' type='hidden' method='POST' value='<?php echo $data['ID'];?>'>
                    <input class='cross' type='image' src='assets/cross.png' method='POST' action='forms/delete.php' value='submit' name='save'
                    />
                </form>
            </div>
            <?php   }
        }
         ?>

            
    </div>
</body>

</html>